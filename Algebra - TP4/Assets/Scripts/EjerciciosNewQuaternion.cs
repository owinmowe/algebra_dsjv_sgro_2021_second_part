﻿using System.Collections.Generic;
using EjerciciosAlgebra;
using UnityEngine;

public class EjerciciosNewQuaternion : MonoBehaviour
{
    enum Ejercicio { Uno, Dos, Tres}
    [SerializeField] Ejercicio ejercicio = Ejercicio.Uno;
    [SerializeField] float angle = 0;
    // Start is called before the first frame update
    void Start()
    {
        VectorDebugger.EnableCoordinates();
        VectorDebugger.EnableEditorView();
        VectorDebugger.AddVector(new Vector3(10f, 0.0f, 0.0f), Color.red, "Vector 1");
        List<Vector3> positions1 = new List<Vector3>();
        positions1.Add(new Vector3(10f, 0.0f, 0.0f));
        positions1.Add(new Vector3(10f, 10f, 0.0f));
        positions1.Add(new Vector3(20f, 10f, 0.0f));
        VectorDebugger.AddVectorsSecuence(positions1, false, Color.blue, "Vector 2");
        List<Vector3> positions2 = new List<Vector3>();
        positions2.Add(new Vector3(10f, 0.0f, 0.0f));
        positions2.Add(new Vector3(10f, 10f, 0.0f));
        positions2.Add(new Vector3(20f, 10f, 0.0f));
        positions2.Add(new Vector3(20f, 20f, 0.0f));
        VectorDebugger.AddVectorsSecuence(positions2, false, Color.yellow, "Vector 3");
    }

    // Update is called once per frame
    void Update()
    {
        switch (ejercicio)
        {
            case Ejercicio.Uno:
                Vector3 newDirection = NewQuaternion.Euler(0, angle * Time.deltaTime, 0) * VectorDebugger.GetVectorsPositions("Vector 1")[1];
                VectorDebugger.UpdatePosition("Vector 1", newDirection);
                break;
            case Ejercicio.Dos:
                List<Vector3> newDirections2 = new List<Vector3>();
                for (int index = 0; index < VectorDebugger.GetVectorsPositions("Vector 2").Count; ++index)
                    newDirections2.Add(NewQuaternion.Euler(new Vector3(0.0f, angle * Time.deltaTime, 0.0f)) * VectorDebugger.GetVectorsPositions("Vector 2")[index]);
                VectorDebugger.UpdatePositionsSecuence("Vector 2", newDirections2);
                break;
            case Ejercicio.Tres:
                List<Vector3> newDirections3 = new List<Vector3>();
                newDirections3.Add(VectorDebugger.GetVectorsPositions("Vector 3")[0]);
                newDirections3.Add(NewQuaternion.Euler(new Vector3(angle * Time.deltaTime, angle * Time.deltaTime, 0.0f)) * VectorDebugger.GetVectorsPositions("Vector 3")[1]);
                newDirections3.Add(VectorDebugger.GetVectorsPositions("Vector 3")[2]);
                newDirections3.Add(NewQuaternion.Euler(new Vector3(-angle * Time.deltaTime, -angle * Time.deltaTime, 0.0f)) * VectorDebugger.GetVectorsPositions("Vector 3")[3]);
                newDirections3.Add(VectorDebugger.GetVectorsPositions("Vector 3")[4]);
                VectorDebugger.UpdatePositionsSecuence("Vector 3", newDirections3);
                break;
        }
    }
}
