﻿using System;
using UnityEngine;
using UnityEngine.Internal;

public struct NewQuaternion : IEquatable<NewQuaternion>
{

    public float x;
    public float y;
    public float z;
    public float w;
    public const float kEpsilon = 1E-06f;

    public Vector3 eulerAngles {
        get => ToEuler();
        set => this = Euler(value);
    }

    public static NewQuaternion identity => new NewQuaternion(0, 0, 0, 1);

    public NewQuaternion normalized => Normalize(this);

    public float this[int index]
    {
        get
        {
            switch (index)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                case 3:
                    return w;
                default:
                    return 0;
            }
        }
        set
        {
            switch (index)
            {
                case 0:
                    x = value;
                    break;
                case 1:
                    y = value;
                    break;
                case 2:
                    z = value;
                    break;
                case 3:
                    w = value;
                    break;
            }
        }
    }

    public NewQuaternion(Vector3 v, float w)
    {
        x = v.x;
        y = v.y;
        z = v.z;
        this.w = w;
    }

    public NewQuaternion(float x, float y, float z, float w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public void Set(float newX, float newY, float newZ, float newW)
    {
        x = newX;
        y = newY;
        z = newZ;
        w = newW;
    }

    public static NewQuaternion operator *(NewQuaternion lhs, NewQuaternion rhs)
    {
        NewQuaternion q;

        q.x = lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y;
        q.y = lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z;
        q.z = lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x;
        q.w = lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z;
        return q;
    }

    public static Vector3 operator *(NewQuaternion rotation, Vector3 point)
    {
        float num1 = rotation.x * 2f;
        float num2 = rotation.y * 2f;
        float num3 = rotation.z * 2f;
        float num4 = rotation.x * num1;
        float num5 = rotation.y * num2;
        float num6 = rotation.z * num3;
        float num7 = rotation.x * num2;
        float num8 = rotation.x * num3;
        float num9 = rotation.y * num3;
        float num10 = rotation.w * num1;
        float num11 = rotation.w * num2;
        float num12 = rotation.w * num3;
        Vector3 vector3;
        vector3.x = ((1f - (num5 + num6)) * point.x + (num7 - num12) * point.y + (num8 + num11) * point.z);
        vector3.y = ((num7 + num12) * point.x + (1f - (num4 + num6)) * point.y + (num9 - num10) * point.z);
        vector3.z = ((num8 - num11) * point.x + (num9 + num10) * point.y + (1f - (num4 + num5)) * point.z);
        return vector3;
    }

    private static bool IsEqualUsingDot(float dot) => (double)dot > 0.999998986721039;

    public static bool operator == (NewQuaternion lhs, NewQuaternion rhs) => IsEqualUsingDot(Dot(lhs, rhs));

    public static bool operator != (NewQuaternion lhs, NewQuaternion rhs) => !(lhs == rhs);

    public static NewQuaternion Euler(Vector3 euler) => Euler(euler.x, euler.y, euler.z);

    public void Normalize() => this = Normalize(this);

    public override int GetHashCode() => x.GetHashCode() ^ y.GetHashCode() << 2 ^ z.GetHashCode() >> 2 ^ w.GetHashCode() >> 1;

    public override bool Equals(object other) => other is NewQuaternion other1 && Equals(other1);

    public bool Equals(NewQuaternion other) => x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z) && w.Equals(other.w);

    public static float Dot(NewQuaternion a, NewQuaternion b)
    {
        float f = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        return f;
    }

    public static NewQuaternion FromToRotation(Vector3 fromDirection, Vector3 toDirection)
    {
        Vector3 a = Vector3.Cross(fromDirection, toDirection);
        NewQuaternion q;
        q.x = a.x;
        q.y = a.y;
        q.z = a.z;
        q.w = Mathf.Sqrt((fromDirection.magnitude * fromDirection.magnitude) * (toDirection.magnitude * toDirection.magnitude)) + Vector3.Dot(fromDirection, toDirection);
        return q;
    }

    public static NewQuaternion Inverse(NewQuaternion rotation)
    {
        NewQuaternion q;
        q.x = -rotation.x;
        q.y = -rotation.y;
        q.z = -rotation.z;
        q.w = rotation.w;
        return q;
    }

    public static NewQuaternion Slerp(NewQuaternion a, NewQuaternion b, float t)
    {
        if (t > 1) t = 1;
        else if (t < 0) t = 0;
        return SlerpUnclamped(a, b, t);
    }

    public static NewQuaternion SlerpUnclamped(NewQuaternion a, NewQuaternion b, float t)
    {
        // quaternion to return
        NewQuaternion q = new NewQuaternion();
        // Calculate angle between them.
        float cosHalfTheta = a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;
        // if a=b or a=-b then theta = 0 and we can return a
        if (Mathf.Abs(cosHalfTheta) >= 1)
        {
            q.w = a.w; q.x = a.x; q.y = a.y; q.z = a.z;
            return q;
        }
        // Calculate temporary values.
        float halfTheta = Mathf.Acos(cosHalfTheta);
        float sinHalfTheta = Mathf.Sqrt(1 - cosHalfTheta * cosHalfTheta);
        // if theta = 180 degrees then result is not fully defined
        // we could rotate around any axis normal to a or b
        if (Mathf.Abs(sinHalfTheta) < Mathf.Epsilon)
        { // fabs is floating point absolute
            q.w = (a.w / 2 + b.w / 2);
            q.x = (a.x / 2 + b.x / 2);
            q.y = (a.y / 2 + b.y / 2);
            q.z = (a.z / 2 + b.z / 2);
            return q;
        }
        float ratioA = Mathf.Sin((1 - t) * halfTheta) / sinHalfTheta;
        float ratioB = Mathf.Sin(t * halfTheta) / sinHalfTheta;
        //calculate Quaternion.
        q.w = (a.w * ratioA + b.w * ratioB);
        q.x = (a.x * ratioA + b.x * ratioB);
        q.y = (a.y * ratioA + b.y * ratioB);
        q.z = (a.z * ratioA + b.z * ratioB);
        return q;
    }

    public static NewQuaternion Lerp(NewQuaternion a, NewQuaternion b, float t)
    {
        if (t > 1) t = 1;
        else if (t < 0) t = 0;
        return LerpUnclamped(a, b, t);
    }

    public static NewQuaternion LerpUnclamped(NewQuaternion a, NewQuaternion b, float t)
    {
        NewQuaternion q = identity;
        bool positive = Dot(a, b) > 0f;
        if (positive)
        {
            q.x = (1 - t * a.x) + (t * b.x);
            q.y = (1 - t * a.y) + (t * b.y);
            q.z = (1 - t * a.z) + (t * b.z);
            q.w = (1 - t * a.w) + (t * b.w);
        }
        else
        {
            q.x = (1 - t * a.x) - (t * b.x);
            q.y = (1 - t * a.y) - (t * b.y);
            q.z = (1 - t * a.z) - (t * b.z);
            q.w = (1 - t * a.w) - (t * b.w);
        }
        return q;
    }

    public static NewQuaternion AngleAxis(float angle, Vector3 axis)
    {
        if (axis.sqrMagnitude == 0) return identity;
        NewQuaternion q;
        q.x = axis.x * Mathf.Sin(angle / 2);
        q.y = axis.y * Mathf.Sin(angle / 2);
        q.z = axis.z * Mathf.Sin(angle / 2);
        q.w = Mathf.Cos(angle / 2);
        return q;
    }

    public static NewQuaternion LookRotation(Vector3 forward, [DefaultValue("Vector3.up")] Vector3 upwards)
    {
        forward = Vector3.Normalize(forward);
        Vector3 right = Vector3.Normalize(Vector3.Cross(upwards, forward));
        upwards = Vector3.Cross(forward, right);
        float m00 = right.x;
        float m01 = right.y;
        float m02 = right.z;
        float m10 = upwards.x;
        float m11 = upwards.y;
        float m12 = upwards.z;
        float m20 = forward.x;
        float m21 = forward.y;
        float m22 = forward.z;

        float num8 = (m00 + m11) + m22;
        NewQuaternion q = new NewQuaternion();
        if (num8 > 0f)
        {
            float num = Mathf.Sqrt(num8 + 1f);
            q.w = num * 0.5f;
            num = 0.5f / num;
            q.x = (m12 - m21) * num;
            q.y = (m20 - m02) * num;
            q.z = (m01 - m10) * num;
            return q;
        }
        if ((m00 >= m11) && (m00 >= m22))
        {
            float num7 = Mathf.Sqrt(((1f + m00) - m11) - m22);
            float num4 = 0.5f / num7;
            q.x = 0.5f * num7;
            q.y = (m01 + m10) * num4;
            q.z = (m02 + m20) * num4;
            q.w = (m12 - m21) * num4;
            return q;
        }
        if (m11 > m22)
        {
            float num6 = Mathf.Sqrt(((1f + m11) - m00) - m22);
            float num3 = 0.5f / num6;
            q.x = (m10 + m01) * num3;
            q.y = 0.5f * num6;
            q.z = (m21 + m12) * num3;
            q.w = (m20 - m02) * num3;
            return q;
        }
        float num5 = Mathf.Sqrt(((1f + m22) - m00) - m11);
        float num2 = 0.5f / num5;
        q.x = (m20 + m02) * num2;
        q.y = (m21 + m12) * num2;
        q.z = 0.5f * num5;
        q.w = (m01 - m10) * num2;
        return q;
    }

    public void SetLookRotation(Vector3 view)
    {
        Vector3 up = Vector3.up;
        SetLookRotation(view, up);
    }

    public void SetLookRotation(Vector3 view, Vector3 up) => this = LookRotation(view, up);

    public static float Angle(NewQuaternion a, NewQuaternion b)
    {
        float num = Dot(a, b);
        return IsEqualUsingDot(num) ? 0.0f : (Mathf.Acos(Mathf.Min(Mathf.Abs(num), 1f)) * 2.0f * Mathf.Rad2Deg); 
    }

    public void ToAngleAxis(out float angle, out Vector3 axis)
    {
        angle = 2 * Mathf.Acos(w) * Mathf.Rad2Deg;
        axis.x = x / Mathf.Sqrt(1 - w * w);
        axis.y = y / Mathf.Sqrt(1 - w * w);
        axis.z = z / Mathf.Sqrt(1 - w * w);
    }

    public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection) => this = FromToRotation(fromDirection, toDirection);

    public static NewQuaternion RotateTowards( NewQuaternion from, NewQuaternion to, float maxDegreesDelta)
    {
        float num = Angle(from, to);
        return Slerp(from, to, maxDegreesDelta / num);
    }

    public static NewQuaternion Normalize(NewQuaternion q)
    {
        float num = Mathf.Sqrt(Dot(q, q));
        return num < kEpsilon ? identity : new NewQuaternion(q.x / num, q.y / num, q.z / num, q.w / num);
    }

    public static NewQuaternion Euler(float x, float y, float z)
    {
        float num1 = Mathf.Cos(y / 2);
        float num2 = Mathf.Cos(z / 2);
        float num3 = Mathf.Cos(x / 2);
        float num4 = Mathf.Sin(y / 2);
        float num5 = Mathf.Sin(z / 2);
        float num6 = Mathf.Sin(x / 2);
        NewQuaternion q;
        q.w = num1 * num2 * num3 - num4 * num5 * num6;
        q.x = num4 * num5 * num3 + num1 * num2 * num6;
        q.y = num4 * num2 * num3 + num1 * num5 * num6;
        q.z = num1 * num5 * num3 - num4 * num2 * num6;
        return q;
    }

    public Vector3 ToEuler()
    {
        float test = x * y + z * w;
        Vector3 v;
        if(test > 0.499f)
        {
            v.y = 2f * Mathf.Atan2(y, x);
            v.x = Mathf.PI / 2;
            v.z = 0;
            return v * Mathf.Rad2Deg;
        }
        if (test > 0.499f)
        {
            v.y = -2f * Mathf.Atan2(y, x);
            v.x = -Mathf.PI / 2;
            v.z = 0;
            return v * Mathf.Rad2Deg;
        }

        v.x = Mathf.Asin(2f * x * z - w * y);
        v.y = Mathf.Atan2(2f * x * w + 2 * y * z, 1 - 2f * (z * z + w * w));
        v.z = Mathf.Atan2(2f * x * y + 2f * z * w, 1 - 2f * (y * y + z * z));

        return v * Mathf.Rad2Deg;
    }

    public static NewQuaternion EulerRotation(float x, float y, float z) => Euler(x, y, z);

    public static NewQuaternion EulerRotation(Vector3 euler) => Euler(euler);

    public void SetEulerRotation(float x, float y, float z) => Euler(x, y, z);

    public void SetEulerRotation(Vector3 euler) => Euler(euler);

    public static NewQuaternion EulerAngles(float x, float y, float z) => Euler(x, y, z);

    public static NewQuaternion EulerAngles(Vector3 euler) => Euler(euler);

    public void ToAxisAngle(out Vector3 axis, out float angle) => ToAngleAxis(out angle, out axis);

    public void SetEulerAngles(float x, float y, float z) => Euler(x, y, z);

    public void SetEulerAngles(Vector3 euler) => Euler(euler);

    public static Vector3 ToEulerAngles(NewQuaternion rotation) => rotation.ToEuler();

    public Vector3 ToEulerAngles() => ToEuler();

    public override string ToString() { return string.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", x, y, z, w); }

    public void SetAxisAngle(Vector3 axis, float angle) => this = AxisAngle(axis, angle);

    public static NewQuaternion AxisAngle(Vector3 axis, float angle) => AngleAxis(angle, axis);
}

/*
    public static NewQuaternion EulerAngles(float x, float y, float z) => Euler(Mathf.Rad2Deg * x, Mathf.Rad2Deg * y, Mathf.Rad2Deg * z);

    public static NewQuaternion EulerAngles(Vector3 euler) => Euler(Mathf.Rad2Deg * euler);

    public void ToAxisAngle(out Vector3 axis, out float angle) => ToAngleAxis(out angle, out axis);

    public void SetEulerAngles(float x, float y, float z) => Euler(Mathf.Rad2Deg * x, Mathf.Rad2Deg * y, Mathf.Rad2Deg * z);

    public void SetEulerAngles(Vector3 euler) => Euler(Mathf.Rad2Deg * euler);

    public static Vector3 ToEulerAngles(NewQuaternion rotation) => rotation.ToEuler();

    public Vector3 ToEulerAngles() => ToEuler();

    public override string ToString() { return string.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", x, y, z, w); }

    public void SetAxisAngle(Vector3 axis, float angle) => this = AxisAngle(axis, angle);

    public static NewQuaternion AxisAngle(Vector3 axis, float angle) => AngleAxis(Mathf.Rad2Deg * angle, axis);

*/
