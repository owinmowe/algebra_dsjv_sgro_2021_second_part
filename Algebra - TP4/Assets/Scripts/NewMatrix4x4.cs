﻿using System;
using UnityEngine;

public struct NewMatrix4x4 : IEquatable<NewMatrix4x4>
{
    public float m00;
    public float m10;
    public float m20;
    public float m30;
    public float m01;
    public float m11;
    public float m21;
    public float m31;
    public float m02;
    public float m12;
    public float m22;
    public float m32;
    public float m03;
    public float m13;
    public float m23;
    public float m33;


    public NewMatrix4x4(Vector4 column0, Vector4 column1, Vector4 column2, Vector4 column3)
    {
        m00 = column0.x;
        m01 = column1.x;
        m02 = column2.x;
        m03 = column3.x;
        m10 = column0.y;
        m11 = column1.y;
        m12 = column2.y;
        m13 = column3.y;
        m20 = column0.z;
        m21 = column1.z;
        m22 = column2.z;
        m23 = column3.z;
        m30 = column0.w;
        m31 = column1.w;
        m32 = column2.w;
        m33 = column3.w;
    }

    public override bool Equals(object other) => other is NewMatrix4x4 other1 && this.Equals(other1);

    public bool Equals(NewMatrix4x4 other)
    {
        int num;
        if (GetColumn(0).Equals(other.GetColumn(0)))
        {
            Vector4 column = GetColumn(1);
            if (column.Equals(other.GetColumn(1)))
            {
                column = GetColumn(2);
                if (column.Equals(other.GetColumn(2)))
                {
                    column = GetColumn(3);
                    num = column.Equals(other.GetColumn(3)) ? 1 : 0;
                    return num != 0;
                }
            }
        }
        num = 0;
        return num != 0;
    }

    public override int GetHashCode()
    {
        Vector4 column = GetColumn(0);
        int hashCode = column.GetHashCode();
        column = GetColumn(1);
        int num1 = column.GetHashCode() << 2;
        int num2 = hashCode ^ num1;
        column = GetColumn(2);
        int num3 = column.GetHashCode() >> 2;
        int num4 = num2 ^ num3;
        column = GetColumn(3);
        int num5 = column.GetHashCode() >> 1;
        return num4 ^ num5;
    }

    public static bool operator == (NewMatrix4x4 lhs, NewMatrix4x4 rhs) => lhs.GetColumn(0) == rhs.GetColumn(0) && lhs.GetColumn(1) == rhs.GetColumn(1) && lhs.GetColumn(2) == rhs.GetColumn(2) && lhs.GetColumn(3) == rhs.GetColumn(3);

    public static bool operator != (NewMatrix4x4 lhs, NewMatrix4x4 rhs) => !(lhs == rhs);

    public static NewMatrix4x4 zero => new NewMatrix4x4(new Vector4(0.0f, 0.0f, 0.0f, 0.0f), new Vector4(0.0f, 0.0f, 0.0f, 0.0f), new Vector4(0.0f, 0.0f, 0.0f, 0.0f), new Vector4(0.0f, 0.0f, 0.0f, 0.0f));

    public static NewMatrix4x4 identity => new NewMatrix4x4(new Vector4(1f, 0.0f, 0.0f, 0.0f), new Vector4(0.0f, 1f, 0.0f, 0.0f), new Vector4(0.0f, 0.0f, 1f, 0.0f), new Vector4(0.0f, 0.0f, 0.0f, 1f));

    public static NewMatrix4x4 operator *(NewMatrix4x4 lhs, NewMatrix4x4 rhs)
    {
        NewMatrix4x4 matrix4x4;
        matrix4x4.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30;
        matrix4x4.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31;
        matrix4x4.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32;
        matrix4x4.m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33;
        matrix4x4.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30;
        matrix4x4.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31;
        matrix4x4.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32;
        matrix4x4.m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33;
        matrix4x4.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30;
        matrix4x4.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31;
        matrix4x4.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32;
        matrix4x4.m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33;
        matrix4x4.m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30;
        matrix4x4.m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31;
        matrix4x4.m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32;
        matrix4x4.m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33;
        return matrix4x4;
    }

    public static Vector4 operator *(NewMatrix4x4 lhs, Vector4 vector)
    {
        Vector4 vector4;
        vector4.x = lhs.m00 * vector.x + lhs.m01 * vector.y + lhs.m02 * vector.z + lhs.m03 * vector.w;
        vector4.y = lhs.m10 * vector.x + lhs.m11 * vector.y + lhs.m12 * vector.z + lhs.m13 * vector.w;
        vector4.z = lhs.m20 * vector.x + lhs.m21 * vector.y + lhs.m22 * vector.z + lhs.m23 * vector.w;
        vector4.w = lhs.m30 * vector.x + lhs.m31 * vector.y + lhs.m32 * vector.z + lhs.m33 * vector.w;
        return vector4;
    }

    public Vector4 GetColumn(int index)
    {
        switch (index)
        {
            case 0:
                return new Vector4(m00, m10, m20, m30);
            case 1:
                return new Vector4(m01, m11, m21, m31);
            case 2:
                return new Vector4(m02, m12, m22, m32);
            case 3:
                return new Vector4(m03, m13, m23, m33);
            default:
                return Vector4.zero;
        }
    }

    public Vector4 GetRow(int index)
    {
        switch (index)
        {
            case 0:
                return new Vector4(m00, m01, m02, m03);
            case 1:
                return new Vector4(m10, m11, m12, m13);
            case 2:
                return new Vector4(m20, m21, m22, m23);
            case 3:
                return new Vector4(m30, m31, m32, m33);
            default:
                return Vector4.zero;
        }
    }

    public float this[int row, int column]
    {
        get => this[row + column * 4, 0];
        set => this[row + column * 4, 0] = value;
    }

    public void SetColumn(int index, Vector4 column)
    {
        this[0, index] = column.x;
        this[1, index] = column.y;
        this[2, index] = column.z;
        this[3, index] = column.w;
    }

    public void SetRow(int index, Vector4 row)
    {
        this[index, 0] = row.x;
        this[index, 1] = row.y;
        this[index, 2] = row.z;
        this[index, 3] = row.w;
    }

    public NewMatrix4x4 inverse => Inverse(this);
    public NewMatrix4x4 transpose => Transpose(this);
    public NewQuaternion rotation
    {
        get
        {
            NewQuaternion q;
            q.w = Mathf.Sqrt(1 + m00 + m11 + m22) / 2;
            q.x = (m21 - m12) / (4 * q.w);
            q.y = (m02 - m20) / (4 * q.w);
            q.z = (m10 - m01) / (4 * q.w);
            return q;
        }
    }
    public Vector3 lossyScale => new Vector3(m00, m11, m22);

    public Vector3 MultiplyPoint(Vector3 point)
    {
        Vector3 vector3;
        vector3.x = (m00 * point.x + m01 * point.y + m02 * point.z) + m03;
        vector3.y = (m10 * point.x + m11 * point.y + m12 * point.z) + m13;
        vector3.z = (m20 * point.x + m21 * point.y + m22 * point.z) + m23;
        float num = 1f / ((m30 * point.x + m31 * point.y + m32 * point.z) + m33);
        vector3.x *= num;
        vector3.y *= num;
        vector3.z *= num;
        return vector3;
    }

    public Vector3 MultiplyPoint3x4(Vector3 point)
    {
        Vector3 vector3;
        Vector3 multVector = MultiplyVector(point);
        vector3.x = multVector.x + m03;
        vector3.y = multVector.y + m13;
        vector3.z = multVector.z + m23;
        return vector3;
    }

    public Vector3 MultiplyVector(Vector3 vector)
    {
        Vector3 vector3;
        vector3.x = m00 * vector.x + m01 * vector.y + m02 * vector.z;
        vector3.y = m10 * vector.x + m11 * vector.y + m12 * vector.z;
        vector3.z = m20 * vector.x + m21 * vector.y + m22 * vector.z;
        return vector3;
    }

    public static NewMatrix4x4 Scale(Vector3 vector)
    {
        NewMatrix4x4 matrix4x4 = identity;
        matrix4x4.m00 = vector.x;
        matrix4x4.m11 = vector.y;
        matrix4x4.m22 = vector.z;
        return matrix4x4;
    }

    public static NewMatrix4x4 Translate(Vector3 vector)
    {
        NewMatrix4x4 matrix4x4 = identity;
        matrix4x4.m03 = vector.x;
        matrix4x4.m13 = vector.y;
        matrix4x4.m23 = vector.z;
        return matrix4x4;
    }

    public static NewMatrix4x4 Rotate(Quaternion q)
    {
        float num1 = q.x * 2f;
        float num2 = q.y * 2f;
        float num3 = q.z * 2f;

        float num4 = q.x * num1;
        float num5 = q.y * num2;
        float num6 = q.z * num3;
        float num7 = q.x * num2;
        float num8 = q.x * num3;
        float num9 = q.y * num3;
        float num10 = q.w * num1;
        float num11 = q.w * num2;
        float num12 = q.w * num3;

        NewMatrix4x4 matrix4x4 = identity;
        matrix4x4.m00 = 1f - (num5 + num6);
        matrix4x4.m10 = num7 + num12;
        matrix4x4.m20 = num8 - num11;
        matrix4x4.m01 = num7 - num12;
        matrix4x4.m11 = 1f - (num4 + num6);
        matrix4x4.m21 = num9 + num10;
        matrix4x4.m02 = num8 + num11;
        matrix4x4.m12 = num9 - num10;
        matrix4x4.m22 = 1f - (num4 + num5);
        return matrix4x4;
    }

    public static NewMatrix4x4 Transpose(NewMatrix4x4 m)
    {
        NewMatrix4x4 matrix4x4;
        matrix4x4.m00 = m.m00;
        matrix4x4.m01 = m.m10;
        matrix4x4.m02 = m.m20;
        matrix4x4.m03 = m.m30;
        matrix4x4.m10 = m.m01;
        matrix4x4.m11 = m.m11;
        matrix4x4.m12 = m.m21;
        matrix4x4.m13 = m.m31;
        matrix4x4.m20 = m.m02;
        matrix4x4.m21 = m.m12;
        matrix4x4.m22 = m.m22;
        matrix4x4.m23 = m.m32;
        matrix4x4.m30 = m.m03;
        matrix4x4.m31 = m.m13;
        matrix4x4.m32 = m.m23;
        matrix4x4.m33 = m.m33;
        return matrix4x4;
    }


    
    public static NewMatrix4x4 Inverse(NewMatrix4x4 m)
    {

        float s0 = m.m00 * m.m11 - m.m10 * m.m01;
        float s1 = m.m00 * m.m12 - m.m10 * m.m02;
        float s2 = m.m00 * m.m13 - m.m10 * m.m03;
        float s3 = m.m01 * m.m12 - m.m11 * m.m02;
        float s4 = m.m01 * m.m13 - m.m11 * m.m03;
        float s5 = m.m02 * m.m13 - m.m12 * m.m03;
        

        float c0 = m.m20 * m.m31 - m.m30 * m.m21;
        float c1 = m.m20 * m.m32 - m.m30 * m.m22;
        float c2 = m.m20 * m.m33 - m.m30 * m.m23;
        float c3 = m.m21 * m.m32 - m.m31 * m.m22;
        float c4 = m.m21 * m.m33 - m.m31 * m.m23;
        float c5 = m.m22 * m.m33 - m.m32 * m.m23;

        float det = (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);

        if(Mathf.Abs(det) < Mathf.Epsilon)
        {
            return zero;
        }
        else
        {
            NewMatrix4x4 matrix4x4;
            matrix4x4.m00 = (m.m11 * c5 - m.m12 * c4 + m.m13 * c3) * 1 / det;
            matrix4x4.m01 = (-m.m01 * c5 + m.m02 * c4 - m.m03 * c3) * 1 / det;
            matrix4x4.m02 = (m.m31 * s5 - m.m32 * s4 + m.m33 * s3) * 1 / det;
            matrix4x4.m03 = (-m.m21 * s5 + m.m22 * s4 - m.m23 * s3) * 1 / det;

            matrix4x4.m10 = (-m.m10 * c5 + m.m12 * c2 - m.m13 * c1) * 1 / det;
            matrix4x4.m11 = (m.m00 * c5 - m.m02 * c2 + m.m03 * c1) * 1 / det;
            matrix4x4.m12 = (-m.m30 * s5 + m.m32 * s2 - m.m33 * s1) * 1 / det;
            matrix4x4.m13 = (m.m20 * s5 - m.m22 * s2 + m.m23 * s1) * 1 / det;

            matrix4x4.m20 = (m.m10 * c4 - m.m11 * c2 + m.m13 * c0) * 1 / det;
            matrix4x4.m21 = (-m.m00 * c4 + m.m01 * c2 + m.m03 * c0) * 1 / det;
            matrix4x4.m22 = (m.m30 * s4 - m.m31 * s2 + m.m33 * s0) * 1 / det;
            matrix4x4.m23 = (-m.m20 * s4 + m.m21 * s2 + m.m23 * s0) * 1 / det;

            matrix4x4.m30 = (-m.m10 * c3 + m.m11 * c1 + m.m12 * c0) * 1 / det;
            matrix4x4.m31 = (m.m00 * c3 - m.m01 * c1 + m.m02 * c0) * 1 / det;
            matrix4x4.m32 = (-m.m30 * c3 + m.m31 * s1 + m.m32 * s0) * 1 / det;
            matrix4x4.m33 = (m.m20 * c3 - m.m21 * s1 + m.m22 * s0) * 1 / det;
            return matrix4x4;
        }
    }

    public static NewMatrix4x4 TRS(Vector3 pos, Quaternion q, Vector3 s)
    {
        NewMatrix4x4 transMat = Translate(pos);
        NewMatrix4x4 rotMat = Rotate(q);
        NewMatrix4x4 scaleMat = Scale(s);

        return transMat * rotMat * scaleMat;
    }

    public void SetTRS(Vector3 pos, Quaternion q, Vector3 s) => this = TRS(pos, q, s);

}
